import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import "jslib/getlog.js" as ApiLog
import UserMetrics 0.1

Page {
	id:_rentalPage
	                   Component {
         id: ensure
         Dialog {
             
             id: rentdia
             title: i18n.tr("Sure?")
             text: i18n.tr("This will rent and unlock the bike, if possible.\nPlease notice that you have to be in front of the bike to lock it again.")
             Button {
                 text: i18n.tr("Continue")
                 color: LomiriColors.green
                 onClicked:{
                     rentalhelper(false);
                     PopupUtils.close(rentdia)
                 }
             }
             Button {
                 text: i18n.tr("Reserve instead")
                 onClicked:{
                     rentalhelper(true);
                     PopupUtils.close(rentdia)
                 }
             }
             Button {
                 text: i18n.tr("Abort")
                 color: LomiriColors.red
                 onClicked:{
                     PopupUtils.close(rentdia);
                 }
             }
         }
    }
	                   Component {
         id: rentocc
         Dialog {
             
             id: rentdia
             title: i18n.tr("Occupied")
             text: i18n.tr("No bike has been rent.\nThe bike has been locked by another user.")
             Button {
                 text: i18n.tr("close")
                 color: LomiriColors.yellow
                 onClicked: PopupUtils.close(rentdia)
             }
         }
    }
         Component {
         id: rentno
         Dialog {
             
             id: rentdia
             title: i18n.tr("Failed")
             text: i18n.tr("No bike has been rent.\nPlease check if this was the correct number and you are logged in correctly.")
             Button {
                 text: i18n.tr("close")
                 color: LomiriColors.orange
                 onClicked: PopupUtils.close(rentdia)
             }
         }
    }
    Component {
         id: rentyes
         Dialog {
             
             id: rentdia
             title: i18n.tr("Bike Rent")
             text: i18n.tr("The bike should now be available.\nHave a good and joyfull trip.")
             Button {
                 text: i18n.tr("close")
                 color: LomiriColors.green
                 onClicked: PopupUtils.close(rentdia)
             }
         }
    }
    header: PageHeader {
        id:rentHeader
        title: i18n.tr("Rent Bike")
    }
    
    ListModel {
       id: rentModel
     }

    Component.onCompleted: {
        rentModel.append({ name: i18n.tr("Reserve bike"), action: "reserve"})
        rentModel.append({ name: i18n.tr("Rent & Unlock"), action: "unlock"})
    }

    Column {
        id: rentCloumn
        spacing:units.dp(2)
        width:parent.width

        Label { //An hack to add margin to the column top
            width:parent.width
            height:rentHeader.height *2
        }
        Icon {
          anchors.horizontalCenter: parent.horizontalCenter
          height: Math.min(parent.width/2, parent.height/2)
          width:height
          source:Qt.resolvedUrl("../assets/logo.svg")
          layer.enabled: true
          layer.effect: LomiriShapeOverlay {
              relativeRadius: 0.75
           }
        }
        Label {
            width: parent.width
            font.pixelSize: units.gu(3)
            font.bold: true
            color: theme.palette.normal.backgroundText
            horizontalAlignment: Text.AlignHCenter
            text: root.operating
        }

    }

    LomiriListView {
         anchors {
            top: rentCloumn.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            topMargin: units.gu(2)
         }

         currentIndex: -1
		 clip:true
		
         model :rentModel
         delegate: ListItem {
            ListItemLayout {
			 Icon {
                 width:units.gu(2)
				 SlotsLayout.position: SlotsLayout.Leading;
                 name:model.icon
             }
             title.text : model.name
             Icon {
                 width:units.gu(2)
                 name:"go-to"
             }
            }
            onClicked: {
                //savage of bike number
                if(model.action != ""){
                if(model.action == "reserve"){
                    rentalhelper(true);
                }
                if(model.action == "unlock"){
                    PopupUtils.open(ensure);
                }
                //there is something to do.

         }
         }
         }
    }


function rentalhelper(reserve){
    var informed = false;
                         ApiLog.bike(sets.apikey, root.operating, sets.loginkey, reserve, function(sucess, answer){
                         //console.log("request rent", sets.apikey, root.operating, root.authkey, reserve);
                         if(sucess){
                             if(!informed){
                            //Update Metric
                            metric.increment(1)
                             PopupUtils.open(rentyes);
                             informed = true;
                             ApiLog.current(sets.apikey, root.authkey, function(subsucess, answer){
                                 if(subsucess){
                                     root.collections = answer;
                             }
                                    });
                             
                             }
                             
                         }else{
                            //rentdia.text="...was NOT sucessfull."
                             if(!informed){
                            if(answer == 423){
                                PopupUtils.open(rentocc);
                            }else{
                             PopupUtils.open(rentno);
                            }
                             informed = true;
                             ApiLog.current(sets.apikey, root.authkey, function(subsucess, answer){
                                 if(subsucess){
                                     root.collections = answer;
                             }
                                    });
                             }
                         }
                     });
}
}
/*
 * Copyright (C) 2021  S60W79/ DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 
