/*
 * Copyright (C) 2021  s60w79
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * NUTbikes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; wsets.loginkeyithout even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Lomiri.Components.Popups 1.3
import "jslib/getlog.js" as ApiLog
import UserMetrics 0.1

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'nutbikes.s60w79'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)
    property string authkey : ""
    property var data : {}
    property var collections :[]
    property string operating
    property var url : []
    Settings{
        id:sets
        //preset api key, will be updated automatically in cases it should be changed.
        property string apikey : "rXXqTgQZUPZ89lzB"
        //number, necassary for log in
        property string number : ""
        //pin (6 Digits) necassary for log in
        property string pin : ""
        //this will store the loginkey and will abolish the login with phone and pwd when implemented.
        property string loginkey
        property bool inform : true
    }
Metric { // Define the Metric object.
    property string circleMetric
    id: metric // A name to reference the metric elsewhere in the code. i.e. when updating format values below.
    name: "nextMetric" // This is a unique ID for storing the user metric data
    format: i18n.tr("%1 Bike(s) rent today by NUTbikes🚲") // This is the metric/message that will display "today". Again it uses the string variable that we defined above
    emptyFormat: i18n.tr("No bikes rent today") // This is the metric/message for tomorrow. It will "activate" once the day roles over and replaces "format". Here I have use a simple translatable string instead of a variable because I didn’t need it to change.
    domain: "nutbikes.s60w79" // This is the appname, based on what you have in your app settings. Presumably this is how the system lists/ranks the metrics to show on the lock screen.
}

AdaptivePageLayout {
    Component.onCompleted:{
        sets.apikey = "rXXqTgQZUPZ89lzB"
//         ApiLog.keyget(function(sucess, key){
//             if(sucess){
//             var apik = JSON.parse(key)["apiKey"];
//             if(apik != null && apik != ""){
//                 //update api key
//                 sets.apikey = apik;
//                 
//             }
//             }
//         });
    }
    anchors.fill: parent
    primaryPage: curbikes
    Page {
        anchors.fill: parent
        id:curbikes

         Component {
         id: failOpenDia
         Dialog {
             
             id: rentdia
             title: i18n.tr("Failed")
             text: i18n.tr("The Bike hasn't been unlocked.\nThe reasons might be: The bike isn't rented by you anymore, the lock is already open, your network is not working.")
             Button {
                 text: i18n.tr("close")
                 color: LomiriColors.orange
                 onClicked: PopupUtils.close(rentdia)
             }
         }
    }
    Component {
         id: failLockDia
         Dialog {
             
             id: rentdia
             title: i18n.tr("Failed")
             text: i18n.tr("The Parking mode could not be enabled.\n\nPlease check your network and that the bike isn't already locked.")
             Button {
                 text: i18n.tr("close")
                 color: LomiriColors.orange
                 onClicked: PopupUtils.close(rentdia)
             }
         }
    }
    Component {
         id: sucOpenDia
         Dialog {
             
             id: rentdia
             title: i18n.tr("Unlocked")
             text:  i18n.tr("The Bike should now be unlocked.\n\nIf not, ensure, that the lock isn't stucking.\nA stucking lock should trigger an automatic return. If not, you should ask the support about.")
             Button {
                 text: i18n.tr("close")
                 color: LomiriColors.green
                 onClicked: PopupUtils.close(rentdia)
             }
         }
    }
           Component {
         id: sucLockDia
         Dialog {
             
             id: rentdia
             title: i18n.tr("Locked")
             text:  i18n.tr("The Parking mode is enabled.\n\nYou can now lock the bike and it will stay reserved for you.")
             Button {
                 text: i18n.tr("close")
                 color: LomiriColors.green
                 onClicked: PopupUtils.close(rentdia)
             }
         }
    } 
        Connections {
    target: UriHandler

    onOpened: {
        root.url = uris;
                   if (root.url.length > 0) {
            var informed = false;
            var nr = "";
            if(root.url[0].indexOf("http://") != -1){
                nr = root.url[0].replace("http://nxtb.it/", "");
            }else{
                nr = root.url[0].replace("https://nxtb.it/", "");
            }
            console.log("Uri REDICT:", nr);
            root.operating = nr;
            curbikes.pageStack.addPageToNextColumn(curbikes, Qt.resolvedUrl("rentalDetails.qml"));
                 }   
       
            
        }
    }
        Component.onCompleted:{
            //check if a nxtb.it url is in the arguments
    if (Qt.application.arguments && Qt.application.arguments.length > 0) {
        for (var i = 0; i < Qt.application.arguments.length; i++) {
            if (Qt.application.arguments[i].match(/^http/)) {
                console.log("Incoming Call on Closed App")
                root.url.push(Qt.application.arguments[i]);
                if (root.url.length > 0) {
            var informed = false;
            var nr = "";
            if(root.url[0].indexOf("http://") != -1){
                nr = root.url[0].replace("http://nxtb.it/", "");
            }else{
                nr = root.url[0].replace("https://nxtb.it/", "");
            }
            console.log("Uri REDICT:", nr);
            root.operating = nr;
            curbikes.pageStack.addPageToNextColumn(curbikes, Qt.resolvedUrl("rentalDetails.qml"));
                 }  
            }
        }
    }
            //info dialog
            if(sets.inform){
            PopupUtils.open(startdia);
        } 
        }

         Component {
         id: startdia
         Dialog {
             
             id: startdia
             title: i18n.tr("Welcome")
             text: i18n.tr("... to this new NUTbikes Version.\nThere are 3 more major Features implemented: Reserving, Parking and unlocking bikes. Reservation works in the renting menu, locking and unlocking with swiping rented bikes in the list to the left.\nWhen Parking or unlocking a bike, you should wait about half a minute before locking or parking it again. Reserving works, but after reserving, sometimes returning it manually doesn't work. Don't panic then, after 10 minutes the bike will be returned automatically.\nYou shouldn't reserve bikes out of service areas, because that might cause a service fee - canceled reservations seems like returnals out of a service area and might cause service fees. If that happens, write a ticket to nextbike, they probably will undo the fee.\nAgain, if anything goes totally wrong, call the service and pretent to not know anything about technique (;")
             Button {
                 text: i18n.tr("Report Errors")
                 color: LomiriColors.red
                 onClicked: Qt.openUrlExternally("https://gitlab.com/S60W79/nutbikes/-/issues")
             }
             Button {
                 text: i18n.tr("App works fine in...")
                 onClicked: Qt.openUrlExternally("https://gitlab.com/S60W79/nutbikes/-/issues/2")
             }
             Button {
                 text: i18n.tr("Help translating")
                 onClicked: Qt.openUrlExternally("https://gitlab.com/S60W79/nutbikes/-/issues/1")
             }
             Button {
                 text: i18n.tr("close")
                 color: LomiriColors.green
                 onClicked: PopupUtils.close(startdia)
             }
             Button {
                 text: i18n.tr("close&not show again")
                 
                 onClicked:{PopupUtils.close(startdia)
                     sets.inform = false;
                 }
             }
         }
    }
        //anchors.fill: parent
        //id:curbikes
        //Component.onCompleted:{
            //if(sets.inform){
            //PopupUtils.open(startdia);
        //} 
        //}
        
        header: PageHeader {
            id: header
            title: i18n.tr('Next UT bikes')
            leadingActionBar {
						actions : [
							Action {
								text : i18n.tr("NUTbikes")
								iconSource : Qt.resolvedUrl("../assets/logo.svg")
								onTriggered : {
                                    curbikes.pageStack.addPageToNextColumn(curbikes, Qt.resolvedUrl("Information.qml"));
								}
							}
						]
					}
					
                    trailingActionBar  { 
						numberOfSlots: 2
						actions : [
							Action {
								text : i18n.tr("Account information")
								iconName : "info"
								onTriggered : {
									curbikes.pageStack.addPageToNextColumn(curbikes, ainfo);
								}
							},
							Action {
								text : i18n.tr("Account")
								iconName : "settings"
								onTriggered : {
									curbikes.pageStack.addPageToNextColumn(curbikes, account);
                                    //curbikes.pageStack.removePages(curbikes);
								}
							}
						]
					}
        }
        
        //pseudo labels, just for correct translation, ALL invisible...
        Label{
            id:text1
            visible:false;
            text: i18n.tr('Duration of rental:')
        }
        
        
        
        //end of pseudo labels
Rectangle {
    color: Qt.rgba(0.27, 0.27, 0.27);
    id:mainbike
             anchors {
                top: header.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
    
    width: 200; height: 200
    ListModel {
        id: bikes
        
    }
    Component {
                
        id: fruitDelegate
        ListItem {
            leadingActions: ListItemActions {
        actions: [
            Action {
                iconName: "redo"
                onTriggered:{
                    root.operating = bikenr;
                    curbikes.pageStack.addPageToNextColumn(curbikes, retdia);
                    
                    
                }
            }
        ]
    }
             trailingActions: ListItemActions {
        actions: [
            Action {
                iconName: "lock"
                onTriggered:{
                    //lock or unlock the lock
                    var state;
                    for(var i = 0; i < root.collections.length; i++){
                        //search for the given bikenr and read if the bike is (already) locked
                        if(root.collections[i]["bike"] == bikenr)state = root.collections[i]["framelock_locked"];
                    }
                    console.log("status", state);
                     if(state){
                         //the bike is locked -> unlock
                         ApiLog.open(sets.apikey, bikenr, sets.loginkey, function(success, sheet){
                             console.log("opened",sheet);
                             if(success){
                                 //opening sucessfull
                                 PopupUtils.open(sucOpenDia);
                                 ApiLog.current(sets.apikey, sets.loginkey, function(subsucess, answer){
                                 if(subsucess){
                                     root.collections = answer;
                                     bikes.clear();
                                    appendhelper();
                             }
                                    });
                             }else{
                                 PopupUtils.open(failOpenDia);
                             }
                         });
                     }else{
                         //the bike is unlocked -> locked
                         ApiLog.lock(sets.apikey, bikenr, sets.loginkey, function(success, sheet){
                             console.log("locked",sheet);
                             if(success){
                                 PopupUtils.open(sucLockDia);
                            ApiLog.current(sets.apikey, sets.loginkey, function(subsucess, answer){
                                 if(subsucess){
                                     root.collections = answer;
                                     bikes.clear();
                                    appendhelper();
                             }
                                    });
                             }else{
                                 PopupUtils.open(failLockDia);
                             }
                         });
                     }
                    
                }
            }
        ]
    }

        ListItemLayout {
        id: layout
        title.text: i18n.tr("Bike number: ")+bikenr;
        subtitle.text: counter
        
    }
        }
    }
    ListView {
        anchors.fill: parent
        model: bikes
        delegate: fruitDelegate
        
        Timer {
    id: timer
    interval: 15000; repeat: true
    running: true
    triggeredOnStart: true
    onTriggered: {
       
        
        bikes.clear();
        appendhelper();
//                  if(sets.loginkey == "" && sets.number != ""){
//                 ApiLog.logdata(sets.apikey, sets.number, sets.pin, function(sucess, sheet){
//                     if(sucess){
//                         root.data=sheet;
//                         sets.loginkey=sheet["user"]["loginkey"];
//                         
//                     
//                         var usr = sheet["user"];
//                         var act = "";
//                         if(usr["active"]){
//                             act=i18n.tr("YES ✔️");
//                             activList.color="green";
//                         }else{
//                             act=i18n.tr("NO ❌");
//                             activList.color="red";
//                         }
//                         status1List.visible=true;
//                         status2List.visible=false;
//                         numberList.text=usr["mobile"];
//                         userList.text = usr["screen_name"];
//                         activList.text = act;
//                         creditList.text = (usr["credits"])/100+usr["currency"];
//                         minutesList.text = (usr["free_seconds"]/60).toFixed(2);
//                         
//                     }
//                 });
//             }else{
//                     //in the first login numer+pwd aren't available. Showing the error message would irritate the user.
//                     
//                     if(root.data !={}){
//                       var sheet=root.data;
//                         sets.loginkey=sheet["user"]["loginkey"];
//                         sets.loginkey = sets.loginkey;
//                     
//                         var usr = sheet["user"];
//                         var act = "";
//                         if(usr["active"]){
//                             act=i18n.tr("YES ✔️");
//                             activList.color="green";
//                         }else{
//                             act=i18n.tr("NO ❌");
//                             activList.color="red";
//                         }
//                         status1List.visible=true;
//                         status2List.visible=false;
//                         numberList.text=usr["mobile"];
//                         
//                         activList.text = act;
//                         creditList.text = (usr["credits"])/100+usr["currency"];
//                         minutesList.text = (usr["free_seconds"]/60).toFixed(2);
//                         screenList.text = usr["screen_name"];
//                       
//                     }else{
//                         status1List.visible=false;
//                         status2List.visible=true;
//                     }
//        }
         ApiLog.current(sets.apikey, sets.loginkey, function(subsucess, answer){
                                 if(subsucess){
                                     root.collections = answer;
                             }
        });
        
}
}
    }
}
        Label {
            id:mainarea
            anchors {
                top: header.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            text: i18n.tr('Swipe up to rent a bike')
            
            verticalAlignment: Label.AlignVCenter
            horizontalAlignment: Label.AlignHCenter
        }
        
        	BottomEdge {
    id: bottomEdge
    height: parent.height
    hint.text: i18n.tr("Rent a bike")
    hint.status:BottomEdgeHint.Active
    contentComponent: Rectangle {
        
    
        width: bottomEdge.width
        height: bottomEdge.height
         color: Qt.rgba(0.27, 0.27, 0.27);
        PageHeader {
            id:header3
            title: i18n.tr("Rent Bikes");
        }
        Column {
            Component.onCompleted:{
                if(sets.loginkey == "" && sets.number != ""){
                }
            }
                
                Component {
         id: chDia
         Dialog {
             
             id: chDia
             title: i18n.tr("Rent")
             text: i18n.tr("How do you want to continue?")
             Button {
                 text: i18n.tr("Reserve")
                 color: LomiriColors.green
                 onClicked:{
                     var informed = false;
                     ApiLog.bike(sets.apikey, bikenr.text, sets.loginkey, true, function(sucess, answer){
                         if(sucess){
                             if(!informed){
                            //Update Metric
                            metric.circleMetric = "%1 Bike(s) rent today by NUTbikes🚲"
                            metric.update(1)
                             PopupUtils.open(curbikes.rentyes);
                             informed = true;
                             ApiLog.current(sets.apikey, sets.loginkey, function(subsucess, answer){
                                 if(subsucess){
                                     root.collections = answer;
                             }
                                    });
                             
                             }
                             
                         }else{
                            //rentdia.text="...was NOT sucessfull."
                             if(!informed){
                            if(answer == 423){
                                PopupUtils.open(root.rentocc);
                            }else{
                             PopupUtils.open(root.rentno);
                            }
                             informed = true;
                             ApiLog.current(sets.apikey, sets.loginkey, function(subsucess, answer){
                                 if(subsucess){
                                     root.collections = answer;
                             }
                                    });
                             }
                         }
                     });
                     PopupUtils.close(chDia);
                 }
             }
             Button {
                 text: i18n.tr("Unlock")
                 color: LomiriColors.green
                 onClicked:{
                     var informed = false;
                     ApiLog.bike(sets.apikey, bikenr.text, sets.loginkey, false, function(sucess, answer){
                         if(sucess){
                             if(!informed){
                            //Update Metric
                            metric.circleMetric = "%1 Bike(s) rent today by NUTbikes🚲"
                            metric.update(1)
                             PopupUtils.open(rentyes);
                             informed = true;
                             ApiLog.current(sets.apikey, sets.loginkey, function(subsucess, answer){
                                 if(subsucess){
                                     root.collections = answer;
                             }
                                    });
                             
                             }
                             
                         }else{
                            //rentdia.text="...was NOT sucessfull."
                             if(!informed){
                            if(answer == 423){
                                PopupUtils.open(rentocc);
                            }else{
                             PopupUtils.open(rentno);
                            }
                             informed = true;
                             ApiLog.current(sets.apikey, sets.loginkey, function(subsucess, answer){
                                 if(subsucess){
                                     root.collections = answer;
                             }
                                    });
                             }
                         }
                     });
                     PopupUtils.close(chDia);
                 }
             }
             Button {
                 text: i18n.tr("Abort")
                 color: LomiriColors.red
                 onClicked:{
                     PopupUtils.close(chDia);
                 }
             }
         }
    }
            id:mainarea2
            anchors {
                top: header3.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
                margins:units.gu(0.5)
            }
            spacing:units.gu(0.2)
            Row{
            TextField {
			id:bikenr
			property bool isLegitimate : true
			property bool isURL : false
			placeholderText: i18n.tr("Bike Number (Below QR Code)")
            }
            Button {
                 iconName: "keyboard-enter"
                 color: LomiriColors.green
                 width:units.gu(5)
                 onClicked:{
                     root.operating = bikenr.text
                     curbikes.pageStack.addPageToNextColumn(curbikes, Qt.resolvedUrl("rentalDetails.qml"));
                 }
             }
            }
            Label{
                text:i18n.tr("\nTo rent bikes via QR codes,\n(install and then) open the\n")
            }
            Button {
                 text: "Tagger-App"
                 onClicked:{
                     Qt.openUrlExternally("https://open-store.io/app/openstore.tagger")
                 }
             }
              Label{
                text:i18n.tr("After scanning the QR code with the app\nuse the button 'open url'")
            }
                
        }
    }
    
    
}
    }
    Page{
        
            Component {
         id: yes
         Dialog {
             
             id: yes
             title: i18n.tr("LOGGED IN")
             text: i18n.tr("You logged in successfully. You now can access your account data.")
             Button {
                 text: "OK"
                 color: LomiriColors.green
                 onClicked:{
                     account.pageStack.removePages(account);
                     PopupUtils.close(yes);
                 }
             }
         }
    }
                Component {
         id: no
         Dialog {
             
             id: no
             title: i18n.tr("ERROR")
            text:i18n.tr("The login wasn't sucessfull, Likely because of a wrong username or password.\n Please try another one.")
             Button {
                 text: i18n.tr("close")
                 color: LomiriColors.orange
                 onClicked: PopupUtils.close(no)
             }
         }
    }
        //Account & settings
        anchors.fill: parent
        id:account
        header: PageHeader {
            id: header2
            title: i18n.tr('Account Settings')
            leadingActionBar {
						actions : [
							Action {
								text : i18n.tr("Back")
								iconName:"back"
								onTriggered : {
									account.pageStack.removePages(account);
								}
							}
						]
					}
            
        }

        Column {
            id:mainarea2
            anchors {
                top: header2.bottom
                left: parent.left
                right: parent.right
                bottom: parent.top
                margins:units.gu(0.5)
            }
            spacing:units.gu(1)
            Label{
                id:status
                text: i18n.tr("...")
                Component.onCompleted:{
                    //determine status (loged in and online|offline|not logged in)
                    if(sets.number == "" && sets.loginkey == ""){
                        status.text = i18n.tr("Status: Not logged in");
                    }else{
                        if(sets.loginkey == ""){
                            status.text = i18n.tr("Status: Logged in");
                        }else{
                            status.text = i18n.tr("Status: Logged in; online");
                        }
                    }
                }
            }
            TextField {
			id:user
			placeholderText: i18n.tr("username/Mobile number")
		}
		TextField {
			id:password
			echoMode:TextInput.Password
			placeholderText: i18n.tr("password")
		}
		Button {
			id:commit
			width:units.gu(5)
			text : i18n.tr("login")
			iconName:  "ok"
            color: LomiriColors.green
			onClicked : {
                var informed = false;
                ApiLog.logdata(sets.apikey, user.text, password.text, function(sucess, sheet){
                    if(sucess){
                        sets.loginkey = sheet["user"]["loginkey"];
                        root.data = sheet;
                        if(!informed){
                        PopupUtils.open(yes);
                        informed = true;
                        }
                    }else{
                        if(!informed){
                        console.log(sheet);
                        PopupUtils.open(no);
                        informed = true;
                        }
                    }
                 informed = true;   
            });
            }
		}
		Button {
			id:adelete
			width:units.gu(42)
			text : i18n.tr("LOG OUT")
            color: LomiriColors.red
			onClicked : {
                sets.number = "";
                sets.pin = "";
                sets.loginkey = "";
                status.text = i18n.tr("Status: Not logged in");
            }
            }
        }
    }
        Page{
        //Account & settings
        anchors.fill: parent
        id:ainfo
        header: PageHeader {
            id: header4
            title: i18n.tr('Account information')
            leadingActionBar {
						actions : [
							Action {
								text : i18n.tr("Back")
								iconName:"back"
								onTriggered : {
									ainfo.pageStack.removePages(ainfo);
								}
							}
						]
					}
					trailingActionBar  { 
						numberOfSlots: 1
						actions : [
							Action {
								text : i18n.tr("Refresh")
								iconName : "view-refresh"
								onTriggered : {
                    
                 ApiLog.userData(sets.apikey, sets.loginkey, function(sucess, sheet){
                     if(sucess){
                         root.data=sheet;
                         var usr = sheet["user"];
                         var act = "";
                         if(usr["active"]){
                             act=i18n.tr("YES ✔️");
                             activList.color="green";
                         }else{
                             act=i18n.tr("NO ❌");
                             activList.color="red";
                         }
                         status1List.visible=true;
                         status2List.visible=false;
                         numberList.text=usr["mobile"];
                         
                         activList.text = act;
                         creditList.text = (usr["credits"])/100+usr["currency"];
                         minutesList.text = (usr["free_seconds"]/60).toFixed(2);
                         screenList.text = usr["screen_name"];
                         
                     }
                 });
									
								}
							}
						]
					}
        }
        
        Column {
            anchors {
                top: header4.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
                margins:units.gu(0.5)
            }
            spacing:units.gu(0.7)
    
        Row{
            Label{
                text:i18n.tr("Status: ")
            }
            Label{
                id:status1List
                text:i18n.tr("Logged in")
                
            }
            Label{
                id:status2List
                text:i18n.tr("NOT Logged in")
                
            }
        }
    
    
        Row{
            Label{
                text:i18n.tr("Mobile Number: ")
            }
            Label{
                id:numberList
            }
        }
    
    
        Row{
            Label{
                text:i18n.tr("Account active: ")
            }
            Label{
                id:activList
            }
        }
    
    
        Row{
            Label{
                text:i18n.tr("Credit: ")
            }
            Label{
                id:creditList
            }
        }
    
    
        Row{
            Label{
                text:i18n.tr("Free Minutes: ")
            }
            Label{
                id:minutesList
            }
        }
         Row{
            Label{
                text:i18n.tr("Screen Name: ")
            }
            Label{
                id:screenList
            }
        }
        Label{
            font.bold:true
            text:i18n.tr("\nAccount sharing")
        }
        Label{
            width:parent.width
            wrapMode: Text.WordWrap
            text:i18n.tr("The opensource service bikes.dvb.solutions allows you to share your account with other users, over a website (so no app necassary). Everyone with the sharing link has access to your account, so do use it with care.\nIf you want to terminate the validaty of the link, just log in again. This will invalidate the link and generate a new one.")
        }
        Button {
            width:parent.width
            text: i18n.tr("Copy Sharing link")
            color: LomiriColors.ash
            iconName:"edit-copy"
            onClicked:Clipboard.push("https://bikes.dvb.solutions#"+sets.loginkey)
        }
        Label{
            width:parent.width
            wrapMode: Text.WordWrap
            font.italic:true
            text:i18n.tr("bikes.dvb.solutions is a foss frontend and collects no data without your knowledge. The Logindata&co is directly sent to nextbike witout proxy.")
        }
        Component.onCompleted:{
         if(sets.loginkey == "" && sets.number != ""){
                ApiLog.userData(sets.apikey, sets.loginkey, function(sucess, sheet){
                    if(sucess){
                        root.data=sheet;
                    
                        var usr = sheet["user"];
                        var act = "";
                        if(usr["active"]){
                            act=i18n.tr("YES ✔️");
                            activList.color="green";
                        }else{
                            act=i18n.tr("NO ❌");
                            activList.color="red";
                        }
                        status1List.visible=true;
                        status2List.visible=false;
                        numberList.text=usr["mobile"];
                        
                        activList.text = act;
                        creditList.text = (usr["credits"])/100+usr["currency"];
                        minutesList.text = (usr["free_seconds"]/60).toFixed(2);
                        screenList.text = usr["screen_name"];
                        
                    }
                });
            }else{
                    //in the first login numer+pwd aren't available. Showing the error message would irritate the user.
                    
                    if(root.data !={}){
                      var sheet=root.data;
                    
                        var usr = sheet["user"];
                        var act = "";
                        if(usr["active"]){
                            act=i18n.tr("YES ✔️");
                            activList.color="green";
                        }else{
                            act=i18n.tr("NO ❌");
                            activList.color="red";
                        }
                        status1List.visible=true;
                        status2List.visible=false;
                        numberList.text=usr["mobile"];
                        
                        activList.text = act;
                        creditList.text = (usr["credits"])/100+usr["currency"];
                        minutesList.text = (usr["free_seconds"]/60).toFixed(2);
                        screenList.text = usr["screen_name"];
                      
                    }else{
                        status1List.visible=false;
                        status2List.visible=true;
                    }
        }
    }
        }

               
        }
        
        
    Page{
        

        //Account & settings
        anchors.fill: parent
        id:retdia
        header: PageHeader {
            id: header6
            title: i18n.tr('Return bike')
            leadingActionBar {
						actions : [
							Action {
								text : i18n.tr("Back")
								iconName:"close"
								onTriggered : {
									retdia.pageStack.removePages(retdia);
								}
							}
						]
					}
        }
Component {
         id: retno
         Dialog {
             
             id: retdia
             title: i18n.tr("Failed")
             text: i18n.tr("No bike has been returned.\nPlease check if the bike already has been returned (+wait 15 seconds) and you are logged in correctly.")
             Button {
                 text: i18n.tr("close")
                 color: LomiriColors.orange
                 onClicked: PopupUtils.close(retdia)
             }
         }
    }
    Component {
         id: retyes
         Dialog {
             
             id: retdia
             title: i18n.tr("Bike returned")
             text: i18n.tr("The bike should now be returned.\nThank you for using the NUTbikes app.")
             Button {
                 text: i18n.tr("close")
                 color: LomiriColors.green
                 onClicked: PopupUtils.close(retdia)
             }
         }
    }
        Column {
            id:mainarea4
            anchors {
                
                top: header6.bottom
                left: parent.left
                right: parent.right
                bottom: parent.top
                margins:units.gu(0.5)
                
            }
            spacing:units.gu(1)
            Label{
                
                text: i18n.tr("Return bike (number: ") +root.operating+")\n";
            }
        Label{
            id:text2
            visible:false;
            text: i18n.tr("please fill out the fields.\n\nIf you return the bike at a station, fill in the station's ID,\n if not fill in the Description of the place you are now\n into 'Place of return'")
        }
        Label{
            id:adHint
            visible:false
                text: i18n.tr("\nAll of thoose fields are optional\n")
            }
        TextField {
			id:statID
			visible:false
			placeholderText: i18n.tr("Station ID")
		}
		TextField {
			id:place
			text:"BIKE "+root.operating
			visible:false
			placeholderText: i18n.tr("place of return")
		}
		Label{
            id:comHint
            visible:false
                text: i18n.tr("\nIf there is something wrong\n(lock broken, dead battery, flat tire, ...) also fulfill this field.\nIf not, ignore")
            }
		TextField {
			id:comment
			visible:false
			placeholderText: i18n.tr("Comment")
		}
		Button{
            id:showAd
            text:i18n.tr("Show Advanced")
            onClicked : {
                if(adHint.visible){
                    //Make opt fields invisible and change button text
                     adHint.visible = false;
                    text2.visible = false;
                    statID.visible = false;
                    place.visible = false;
                    comHint.visible = false;
                    comment.visible = false;
                    showAd.text = i18n.tr("Show Advanced")
                }else{
                    //Make opt fields visible and change button text
                    text2.visible = true;
                    adHint.visible = true;
                    statID.visible = true;
                    place.visible = true;
                    comHint.visible = true;
                    comment.visible = true;
                    showAd.text = i18n.tr("Hide Advanced")
                }
            }
        }
		Button {
			id:commit2
			text : i18n.tr("RETURN BIKE")
            color: LomiriColors.green
			onClicked : {
                var reacted = false;
                // var place;
                // if(place == undefined && statID.text == undefined){
                //     console.log("NULL");
                //     //if no place given, set 0 as place to indicate the API that the bike hasn't been moved.
                //     place = "0";
                // }else{
                //      place = place.text;
                // }
            ApiLog.retbike(sets.apikey, sets.loginkey, comment.text, place.text, statID.text, root.operating, function(sucess, sheet){
                
                if(sucess){
                    if(!reacted){
                    PopupUtils.open(retyes);
                    reacted = true;
                    }
                }else{
                    if(!reacted){
                    PopupUtils.open(retno);
                    reacted = true;
                    }
                }
            });
            }
		}
        }
    }

                }
                            function appendhelper(){
                
            bikes.clear()
            var collections = root.collections;
            if (collections.length > 0){
                mainarea.visible = false;
                mainbike.visible = true;
            }else{
                mainarea.visible = true;
                mainbike.visible = false;
            }
            for(var i = 0; i < collections.length; i++){
            var col = collections[i];
            var nr = col["bike"];
            
            var stempdif = Math.floor(Date.now()/1000-col["start_time"]);
            
            var h = Math.floor(stempdif/60/60);
            var m = Math.floor(stempdif%(60*60)/60);
            var s = Math.floor(stempdif-h*60*60-m*60);
            var hint = "";
            if(col["framelock_locked"]){
                hint = i18n.tr(" ««Locked. Swipe to unlock🔑");
            }else{
                hint = i18n.tr(" ««Swipe to lock🔑");
            }
            bikes.append({"bikenr":nr, "counter":text1.text+h+":"+m+":"+s+hint});
            
            }
            
        
            }
                }
        

