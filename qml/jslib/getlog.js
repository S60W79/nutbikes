function keyget(callback){
    //helper for getting the current api key
    var reacted = false;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "https://webview.nextbike.net/getAPIKey.json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
    if (xhr.status == 200) {
    callback(true, "rXXqTgQZUPZ89lzB");
    }else{
    callback(false, "rXXqTgQZUPZ89lzB");
    }
    };
    xhr.send(); 
}

function logdata(apik, mobile, pin, callback){
console.log("\nmobile:",mobile);
console.log("\npin:",pin)
//get login data
var reacted = false;
var xhr = new XMLHttpRequest();
xhr.open("POST", "https://api.nextbike.net/api/login.json");
xhr.setRequestHeader("Accept", "application/json");
xhr.setRequestHeader("Content-Type", "application/json");
xhr.onreadystatechange = function () {

if (xhr.status == 200) {
var plain =  xhr.responseText;
console.log("\nplain:",plain,"\n");

if (plain != null && plain != ""){
    var sheet = JSON.parse(plain);
    if(!reacted){
        callback(true, sheet);
    reacted = true;
    }
}
}else{
var plain =  xhr.responseText;
console.log("\nplain:",plain,"\n");
if(!reacted){
    callback(false, xhr.status);
}
}
};

var data = `{
"apikey": "`+apik+`",
"mobile": "`+mobile+`",
"pin": "`+pin+`",
"show_errors":1 
}`;
console.log(data);
xhr.send(data); 
}

function bike(apik, bikenr, authkey, reserve, callback){
    
    var reacted = false;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.nextbike.net/api/rent.json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
           console.log("rental", xhr.status);
console.log("rental", xhr.responseText);
    if (xhr.status == 200) {
    var plain =  xhr.responseText;

    if (plain != null && plain != ""){
        var sheet = JSON.parse(plain);
        if(!reacted){
            callback(true, sheet);
        reacted = true;
        }
    }
    }else{
    if(!reacted){
        callback(false, xhr.status);
    }
    }
    };

    var data = `{
    "apikey": "`+apik+`",
    "bike": `+parseInt(bikenr)+`,
    "loginkey": "`+authkey+`",
    "start_paused":`+reserve+`,
    "show_errors":1 
    }`;
    xhr.send(data); 
}


function current(apikey, authkey, callback){
    //rent a bike
    var reacted = false;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.nextbike.net/api/getOpenRentals.json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
    if (xhr.status == 200) {
    console.log(xhr.status);
    var plain =  xhr.responseText;
    console.log("Rentals...", plain);
    if (plain != null && plain != ""){
        var sheet = JSON.parse(plain);
        if(!reacted){
            callback(true, sheet["rentalCollection"]);
        reacted = true;
        }
    }
    }else{
    //console.log("error", xhr.status);
    //console.log("errorcurr...", xhr.responseText);
    if(!reacted){
        if(xhr.status == 0){
        callback(true, []);
        }else{
        callback(false, xhr.status);
        }
    }
    }
    };

    var data = `{
    "apikey": "`+apikey+`",
    "loginkey": "`+authkey+`",
    "show_errors":1 
    }`;
    //console.log(data);
    xhr.send(data); 
}
function retbike(apikey, authkey, comment, place, station, bikenr, callback){
    //return the bike
var reacted = false;
var xhr = new XMLHttpRequest();
xhr.open("POST", "https://api.nextbike.net/api/return.json");
xhr.setRequestHeader("Accept", "application/json");
xhr.setRequestHeader("Content-Type", "application/json");
xhr.onreadystatechange = function () {
if (xhr.status == 200) {
console.log(xhr.status);
var plain =  xhr.responseText;
console.log("RET answer:", plain);
if (plain != null && plain != ""){
    var sheet = JSON.parse(plain);
    if(!reacted){
        callback(true, sheet);
    reacted = true;
    }
}
}else{
console.log("error", xhr.status);
console.log("error", xhr.responseText);
if(!reacted){
    callback(false, xhr.status);
}
}
};

var data = `{
"api_key": "`+apikey+`",
"bike": "`+bikenr+`",
"loginkey": "`+authkey
if(comment != null && comment != ""){
    data = data +`",
"comment":"`+comment;
}
if(place != null && place != ""){
    data = data+`",
"place":"`+place+`"`;
}else{
    data = data+`,
"station":"`+station+`"`;
}
data = data +`,
"show_errors":1 
}`;
console.log(data);
xhr.send(data); 
}
function open(apik, bikenr, authkey, callback){
    
    var reacted = false;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.nextbike.net/api/openLock.json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
    if (xhr.status == 200) {
    var plain =  xhr.responseText;
    if (plain != null && plain != ""){
        var sheet = JSON.parse(plain);
        if(!reacted){
            callback(true, sheet);
        reacted = true;
        }
    }
    }else{
    if(!reacted){
        callback(false, xhr.status);
    }
    }
    };

    var data = `{
    "apikey": "`+apik+`",
    "bike": `+parseInt(bikenr)+`,
    "loginkey": "`+authkey+`",
    "show_errors":1 
    }`;
    xhr.send(data); 
}
function lock(apik, bikenr, authkey, callback){
    
    var reacted = false;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.nextbike.net/api/rentalBreak.json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
    if (xhr.status == 200) {
    var plain =  xhr.responseText;
    console.log("locker", plain);
    if (plain != null && plain != ""){
        var sheet = JSON.parse(plain);
        if(!reacted){
            callback(true, sheet);
        reacted = true;
        }
    }
    }else{
    if(!reacted){
        callback(false, xhr.status);
    }
    }
    };

    var data = `{
    "apikey": "`+apik+`",
    "bike": `+parseInt(bikenr)+`,
    "loginkey": "`+authkey+`",
    "show_errors":1 
    }`;
    xhr.send(data); 
}

function userData(apik, authkey, callback){
    //shows user details without logging in.

var reacted = false;
var xhr = new XMLHttpRequest();
xhr.open("POST", "https://api.nextbike.net/api/getUserDetails.json");
xhr.setRequestHeader("Accept", "application/json");
xhr.setRequestHeader("Content-Type", "application/json");
xhr.onreadystatechange = function () {
if (xhr.status == 200) {

var plain =  xhr.responseText;
console.log("\nplain:",plain,"\n");
if (plain != null && plain != ""){
    var sheet = JSON.parse(plain);
    if(!reacted){
        callback(true, sheet);
    reacted = true;
    }
}
}else{

if(!reacted){
    callback(false, xhr.status);
}
}
};

var data = `{
"apikey": "`+apik+`",
"loginkey": "`+authkey+`",
"show_errors":1 
}`;

xhr.send(data); 
}

